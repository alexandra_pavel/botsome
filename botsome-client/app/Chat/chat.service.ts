import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Constants } from '../app.constants';
import { ChatMessage, ChatResponse } from '../Models/chatMessage';

@Injectable()
export class ChatService{
    private getChatHistoryApi = 
        Constants.ADMIN_SERVER_BASE + "/api/" + Constants.CURRENT_BOT_ID + "/chatMessages";
    private getWeeklyChatHistoryApi = Constants.ADMIN_SERVER_BASE + "/api/" + Constants.CURRENT_BOT_ID + "/weeklyMessages";
    private postChatHistoryApi = Constants.ADMIN_SERVER_BASE + "/api/chatMessages";
    private postChatMessengerApi = Constants.CHAT_SERVER_POST_URL + "/api/chat_messenger";
    private restChatbotApi = Constants.CHAT_SERVER_POST_URL + "/api/reset_chatbot";

    private headers = new Headers({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    });

    constructor (private http: Http) {
    }

    getChatRecentHistory(): Observable<ChatMessage[]>{
         // ...using get request
         return this.http.get(this.getChatHistoryApi)
                .map((res:Response) => res.json())
                .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    getLastWeekChatHistory(): Observable<ChatMessage[]>{
         // ...using get request
         return this.http.get(this.getWeeklyChatHistoryApi)
                .map((res:Response) => res.json())
                .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    sendChatMessage(message: string): Observable<ChatResponse>{
        var data = {
            'message': message
        };

        return this.http.post(
                 this.postChatMessengerApi,
                 JSON.stringify(data),
                {headers: this.headers}
            ).map((res:any) =>res.json());
    }

    resetChatBot(): Observable<any>{
        return this.http.post(
            this.restChatbotApi,
            JSON.stringify({}),
            {headers: this.headers}
        ).map((res:any) => res.json());
    }

    addChatMessageToHistory(chatMsg: ChatMessage): Observable<any>{
        return this.http.post(
            this.postChatHistoryApi,
            JSON.stringify(chatMsg),
            {headers: this.headers}
        ).map((res:any) => res.json());
    }
}