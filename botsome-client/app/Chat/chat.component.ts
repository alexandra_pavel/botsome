import { Component, OnInit } from "@angular/core"

import { ChatService } from "./chat.service";
import { ChatMessage, ChatResponse } from "../Models/chatMessage";
import { Constants } from "../app.constants";

@Component({
    selector: "chat",
    templateUrl: "app/Chat/chat.component.html"
})
export class ChatComponent implements OnInit {

    messages: ChatMessage[];
    errorMessages: string[];
    newMessage: string;
    isResetting: Boolean;
    successfulMsg: string;

    constructor(private chatService: ChatService) {
        this.errorMessages = [];
        this.isResetting = false;
    }

    ngOnInit(){
        this.loadChatHistory();
    }

    loadChatHistory(){
        this.chatService.getChatRecentHistory()
        .subscribe((x: ChatMessage[]) => {
            this.messages = x.sort((a: ChatMessage,b: ChatMessage) =>{
                                        if (a.created_date < b.created_date)
                                            return -1;
                                        if (a.created_date > b.created_date)
                                            return 1;
                                        return 0;
                                    });
        },
        (err)=> {
            this.errorMessages.push(err);
        })
    }

    resetChatbot(){
        this.isResetting = true;
        this.chatService.resetChatBot()
        .subscribe((res) => {
            this.successfulMsg = res;
            this.isResetting = false;
        }, (err)=> {
            this.isResetting = false;
            this.errorMessages.push(err);
        })
    }

    sendMessage(){
        if(!this.newMessage) {
            return;
        }
        var myMessage: ChatMessage = {
            sender: "test_chat",
            recipient: "chatbot",
            bot_id: Constants.CURRENT_BOT_ID,
            created_date: null,
            message: this.newMessage,
            node_result_id: "undefined",
            node_type: "undefined"
        };

        this.messages.push(myMessage);
        this.newMessage = "";
        this.chatService.addChatMessageToHistory(myMessage)
        .subscribe((x)=> {}, (err)=> {this.errorMessages.push(err)});

        this.chatService.sendChatMessage(myMessage.message)
        .subscribe((response: ChatResponse) =>{
            if(response.response_message){
                var newResponse: ChatMessage = {
                    sender: "chatbot",
                    recipient: "test_chat",
                    bot_id: Constants.CURRENT_BOT_ID,
                    created_date: null,
                    message: response.response_message,
                    node_result_id: response.node_result_id,
                    node_type: response.node_type
                };
                this.messages.push(newResponse);
                this.chatService.addChatMessageToHistory(newResponse)
                .subscribe((x)=> {}, (err)=> {this.errorMessages.push(err)});
            }
        });
    }
}