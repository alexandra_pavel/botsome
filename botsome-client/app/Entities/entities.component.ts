import { Component, OnInit } from "@angular/core";
import { FormsModule } from '@angular/forms';
import { EntityService } from "./entities.service";
import { Entity, Value } from "../Models/entity";
import { Constants } from "../app.constants";

@Component({
    selector: "entities",
    templateUrl: "app/Entities/entities.component.html"
})
export class EntitiesComponent implements OnInit{
    entities: Entity[];
    entityCopies: Entity[];
    errorMessages: string[];
    successfulMsg: string;

    constructor(private entityService: EntityService){
        this.errorMessages = [];
        this.entityCopies = [];
        this.successfulMsg = null;
    }

    removeAlert(i:number){
        this.errorMessages.splice(i,1);
    }

    removeSuccessAlert(){
        this.successfulMsg = null;
    }

    ngOnInit(){
        this.loadEntities();
    }

    loadEntities(){
        this.entityService.getEntities()
        .subscribe((entities) => {
            this.entities = entities;

            this.entities.map((entity) =>{
                if(entity.values){
                    entity.values_array =  entity.values.split(';').map(x=> {return {value: x.trim()}});
                }
                return entity;
            })
        }, (err) => {
            this.errorMessages.push(err);
        });
    }

    addNewEntity(){
        var entity = new Entity();
        entity._id = "0";
        entity.isInEditMode = true;
        entity.values_array = [];
        entity.bot_id = Constants.CURRENT_BOT_ID;
        this.entities.unshift(entity);
    }

    addNewValue(entity: Entity){
        var existingNewEntity = entity.values_array.filter(x=> {return x.value == ""});
        if(existingNewEntity.length == 0){
            entity.values_array.unshift({
                value: ""
            });
        }
    }

    removeValue(i:number, entity: Entity){
        entity.values_array.splice(i, 1);
    }

    editEntity(entity: Entity){
        this.entityCopies.push(entity);
        entity.isInEditMode = true;
    }

    saveEntityChanges(entity: Entity){
        //check if entity name already exists
        var existingEntities = this.entities.filter(x=> 
                {
                    return x.name == entity.name && x._id != entity._id
                });
        if(existingEntities.length != 0){
            this.errorMessages.push("Entity " + entity.name +" already exists. Please edit existing record.");
            return;
        }
        if(entity.is_value_restricted)
        {        
            entity.values = entity.values_array.map(x=> {return x.value.trim()}).join(';');
        }else{
            entity.values = "";
        }
        this.entityService.updateEntity(entity)
        .subscribe((res: any) => {
            if(res.newId){
                entity._id = res.newId;
            }else{
                //remove from temporary copies
                var entityCopy = this.entityCopies.filter(x=> {return x._id == entity._id})[0];
                var index = this.entityCopies.indexOf(entityCopy);
                if(index >= 0){
                    this.entityCopies.splice(index, 1);
                }
            }
            this.successfulMsg = "Entity "+ entity.name +  " updated successfully";
            entity.isInEditMode = false;
        },
        (err) => {
            this.errorMessages.push(err);
        });
    }

    removeEntity(entity: Entity){
        this.entityService.deleteEntity(entity._id)
        .subscribe(
            (res) =>{
                var index = this.entities.indexOf(entity);
                if(index >= 0){
                    this.entities.splice(index, 1);
                }
                this.successfulMsg = "Entity " + entity.name + " was successfully removed.";
            },
        (err) => {this.errorMessages.push(err);}
        )
    }

    closeEditEntity(entity: Entity){
        if(entity._id == "0"){
            let index = this.entities.indexOf(entity);
            if(index >= 0){
                this.entities.splice(index,1);
            }
        }else{
            var entityCopy = this.entityCopies.filter(x => {return x._id == entity._id})[0];
            let index = this.entityCopies.indexOf(entityCopy);
            if(index >= 0){
                this.entityCopies.splice(index, 1);
            }
        }
        entity.isInEditMode = false;
    }

}