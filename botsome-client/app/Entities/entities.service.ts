import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Constants } from '../app.constants';
import { Entity, EntityModel } from '../Models/entity';

@Injectable()
export class EntityService {
    getEntitiesApiUrl = 
        Constants.ADMIN_SERVER_BASE + "/api/" + Constants.CURRENT_BOT_ID + "/entities";
    entitiesApiUrl = Constants.ADMIN_SERVER_BASE + "/api/entities";

    headers = new Headers({
        'Content-Type': 'application/json'
    });

    constructor (private http: Http) {}

      getEntities() : Observable<Entity[]> {

         // ...using get request
         return this.http.get(this.getEntitiesApiUrl)
                        // ...and calling .json() on the response to return data
                         .map((res:Response) => res.json())
                         //...errors if any
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

     }

     updateEntity(entity: Entity): Observable<Response>{
        var data = new EntityModel();
        data.bot_id = entity.bot_id;
        data.entity_id = entity.entity_id;
        data.is_value_restricted = entity.is_value_restricted;
        data.values = entity.values;
        data.name = entity.name;

         if(entity._id == "0"){
          return this.http.post(
             this.entitiesApiUrl,
             JSON.stringify(data),
             {headers: this.headers}
            ).map((res: any) => res.json());
         }else{
             data._id = entity._id;
             return this.http.put(
             this.entitiesApiUrl + "/" + entity._id,
             JSON.stringify(data),
             {headers: this.headers}
            ).map((res: any) => res.json());
         }
     }

     deleteEntity(entityId: string): Observable<Response>{
         return this.http.delete(
           this.entitiesApiUrl + "/" + entityId,
             {headers: this.headers}
            ).map((res: any) => res.json());
     }
}