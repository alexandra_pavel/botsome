import { Component, OnInit  } from "@angular/core";
import { ChatMessage } from "../Models/chatMessage";
import { ChatService } from "../Chat/chat.service";

@Component({
    selector: "dashboard",
    templateUrl: "app/Dashboard/dashboard.component.html"
})
export class DashboardComponent implements OnInit{
    messagesCountChart: {};
    intentPriorityChart: {};
    noOfUsersChart: {};
    messages: ChatMessage[];

    constructor(private chatService: ChatService){}

    ngOnInit(){
        this.loadChatMessages();
    }

    loadChatMessages(){
        this.chatService.getLastWeekChatHistory()
        .subscribe((data: ChatMessage[]) => {
            this.messages = data;
           this.setDataForMessagesChart();
        });
    }

    setDataForIntentsChart(){        
            var intentsCount = {};
            var countsIntents = [];
            var labels = [];
    }

    setDataForMessagesChart(){
           var labelsMessages = [];
            var countsMessages = [];
            var messageCounts = {};
            
            this.messages.forEach((msg) => {                
                var month = new Date(msg.created_date).getUTCMonth() + 1; //months from 1-12
                var day = new Date(msg.created_date).getUTCDate();
                var key = day +"/ "+ month;
                if(messageCounts[key])
                    messageCounts[key]++;
                else{
                    messageCounts[key] = 1;
                }
            });

            for(var key in messageCounts) {
                if(messageCounts.hasOwnProperty(key)) {
                    labelsMessages.push(key);
                    countsMessages.push(messageCounts[key]);
                }
            }
            this.loadMessagesCountChart(labelsMessages, countsMessages);
    }
    loadMessagesCountChart(labels: string[], data: number[]){
        this.messagesCountChart = {
            type : 'line',
            data : {
            labels: labels,
            datasets: [
                {
                    label: "ChatBot usage - number or messages",
                    data: data,
                    fillColor: ["rgba(205,64,64,0.5)", "rgba(220,220,220,0.5)", "rgba(24,178,235,0.5)", "rgba(220,220,220,0.5)"],
                    strokeColor: "rgba(220,220,220,0.8)",
                }
            ],
            
            options : {
                responsive: false,
                maintainAspectRatio: true
            }
        }
    }
}