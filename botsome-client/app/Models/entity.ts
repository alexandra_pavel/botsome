export class Entity{
    _id: string;
    bot_id: number;
    entity_id: Number;
    name: string;
    is_value_restricted: Boolean;
    values: string;
    values_array: Value[];
    isInEditMode:Boolean;

    constructor(){
        this.isInEditMode = false;
    }
}

export class EntityModel{
    _id: string;
    bot_id: number;
    entity_id: Number;
    name: string;
    is_value_restricted: Boolean;
    values: string;
}

export interface Value{
    value: string;
}