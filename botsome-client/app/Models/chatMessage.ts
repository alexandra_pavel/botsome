export class ChatMessage {
    sender: string;
    recipient: string;
    message: string;
    node_result_id: string;
    node_type: string;
    created_date: Date;
    bot_id: number;
}

export class ChatResponse{
    response_message: string;
    node_result_id: string;
    node_type: string;
}