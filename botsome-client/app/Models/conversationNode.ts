import { IntentModel } from "./intent";
import { EntityModel } from "./entity";

export class ConversationNode {
    _id : string; 
    bot_id : number; 
    parent_node_id : string; 
    type : string; 
    response : string;
    result_id : string; 
    waits_entity_response : string;
    response_msg_id : string;
    response_type : string;
    treeLevel: number;

    associatedIntent: IntentModel;
    associatedEntity: EntityModel;
    waitsForEntity: EntityModel;
    childNodes: ConversationNode[];
    canBeEntityType: Boolean;
    markedForDeletion: Boolean;

    constructor(){
        this.canBeEntityType = true;
        this.markedForDeletion = false;
    }
}


export class ConversationNodeModel{
    _id             : string; 
    bot_id          : number; 
    parent_node_id  : string; 
    type            : string; 
    response        : string;
    result_id       : string; 
    waits_entity_response : string;    
    response_msg_id : string;
    response_type : string;
}