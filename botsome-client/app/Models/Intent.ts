import { IntentQuestion } from './intentQuestion';

export class Intent {
    _id: string;
    bot_id: number;
    intent_id: number;
    name: string;
    created_date: Date;
    questions: IntentQuestion[];
    isInEditMode: Boolean;

    constructor(){
        this.isInEditMode = false;
    }
}

export class IntentModel{
    _id: string;
    bot_id: number;
    intent_id: number;
    name: string;
    created_date: Date;
}