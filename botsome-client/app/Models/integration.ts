export class Integration{
    _id : string;
    bot_id : number;
    external_post_api_url : string;
    post_message: string;
    type: string;
    name: string;
    isInEditMode: Boolean;
}