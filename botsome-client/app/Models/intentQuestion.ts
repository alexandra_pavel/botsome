export class IntentQuestion {
    _id: string;
    intent_id: number;
    intent_obj_id: string;
    question: string;
    created_date: Date;
    marked_for_deletion: Boolean;
}