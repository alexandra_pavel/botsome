import { Component,
         Input,
         AfterContentInit,
         OnInit,
         EventEmitter,
         Output
        } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ConversationNode } from "../Models/conversationNode";
import { IntentService } from "../Intents/intents.service";
import { EntityService } from "../Entities/entities.service";
import { Intent, IntentModel } from "../Models/intent";
import { Entity, EntityModel, Value } from "../Models/entity";
import { Integration } from "../Models/integration";
import { IntegrationService } from "../ExternalIntegration/integration.service";

@Component({
    selector: "conversation-node",
    templateUrl: "app/ConversationDialog/node.component.html"
})
export class ConversationNodeComponent implements AfterContentInit, OnInit{
    @Input()
    public node: ConversationNode;
    @Input()
    public editMode: Boolean;

    @Output() onNodeRemoved: EventEmitter<any> = new EventEmitter();
    @Output() onNodeAdded:  EventEmitter<any> = new EventEmitter();

    intents: IntentModel[];
    entities: EntityModel[];
    externalSources: Integration[];
    node_types: Value[];
    responseTypes: Value[];
    errorMessages = [];

    constructor(private intentService: IntentService,
                private entityService: EntityService,
                private integrationService: IntegrationService){ 
        this.node_types = [{ value: 'intent' }];      
        this.errorMessages = [];  
        this.responseTypes = [{value: 'text'}, {value: 'external'}];
    }

    ngAfterContentInit(){
        console.log('New node added ' + this.node._id +' and has ' + this.node.childNodes.length +' child nodes.');
    }

    ngOnInit(){
        this.loadIntents();
        this.loadEntities();
        this.loadSources();
        if(this.node.canBeEntityType){
            this.node_types.push({ value: 'entity' });
        }
    }

    loadIntents(){
        this.intentService.getIntents()
        .subscribe(
            (intents: Intent[]) => {
                this.intents = intents.map(x=> {
                    return {
                        _id: x._id,
                        name: x.name,
                        bot_id: x.bot_id,
                        intent_id: x.intent_id,
                        created_date: x.created_date
                    };
                });            
            },
            err => { console.log(err); }
        );
    }

    loadSources(){
        this.integrationService.getIntegrations()
        .subscribe((sources) =>{this.externalSources = sources;},
        (err) => {console.log(err);});
    }
    loadEntities(){
        this.entityService.getEntities()
        .subscribe(
            (entities: Entity[]) => {
                this.entities = entities.map(x=> {
                    return {
                        _id: x._id,
                        bot_id: x.bot_id,
                        entity_id: x.entity_id,
                        name: x.name,
                        is_value_restricted: x.is_value_restricted,
                        values: x.values
                    };
                });
            },
            err=> {console.log(err);}
        );
    }

    addChildNode(){
        var childNode = new ConversationNode();
        if(this.node.childNodes.length == 0){
            childNode.canBeEntityType = true;
        }else {
            childNode.canBeEntityType = false;
            this.node.childNodes[0].canBeEntityType = false;
        }

        childNode.bot_id = this.node.bot_id;
        childNode.childNodes = [];
        childNode.treeLevel = this.node.treeLevel +1;
        childNode.parent_node_id = this.node._id;
        childNode.waits_entity_response = "0";
        this.node.childNodes.unshift(childNode);
        this.onNodeAdded.emit([childNode]);
    }

    removeNode(){         
        this.onNodeRemoved.emit([this.node._id]);
    }

    onChildNodeRemoved($event){
        this.onNodeRemoved.emit([$event[0]]);
    }

    onChildNodeAdded($event){
         this.onNodeAdded.emit([$event[0]]);
    }

    onIntentChanged(intentId){
        this.node.associatedIntent = this.intents.filter(x=> {return x._id == intentId})[0];
    }

    onEntityChanged(entityId){
        this.node.associatedEntity = this.entities.filter(x=> {return x._id == entityId})[0];
    }
}