import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Constants } from '../app.constants';
import { ConversationNode } from '../Models/conversationNode';
import { ConversationNodeModel } from '../Models/conversationNode';

@Injectable()
export class ConversationService {
    getConversationApi = 
        Constants.ADMIN_SERVER_BASE + "/api/"+ Constants.CURRENT_BOT_ID + "/conversationNodes";
    postPutConversationApi = Constants.ADMIN_SERVER_BASE + "/api/conversationNodes";

    headers = new Headers({
        'Content-Type': 'application/json'
    });

    constructor (private http: Http) {

    }

     getAllNodes() : Observable<ConversationNode[]> {

         // ...using get request
         return this.http.get(this.getConversationApi)
                        // ...and calling .json() on the response to return data
                         .map((res:Response) => res.json())
                         //...errors if any
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

    }

    updateNode(node: ConversationNode){
        var data: ConversationNodeModel = {
            _id        : node._id,         
            bot_id     : node.bot_id,
            parent_node_id : node.parent_node_id,
            type           : node.type,
            response       : node.response,
            result_id      : node.result_id,
            waits_entity_response: node.waits_entity_response,
            response_type: node.response_type,
            response_msg_id: node.response_msg_id
        };

        if(data._id <= '0'){
            return this.http.post(
                 this.postPutConversationApi,
                 JSON.stringify(data),
                {headers: this.headers}
            ).map((res:any) =>res.json());
        }else{
            return this.http.put(
                 this.postPutConversationApi + "/" + data._id,
                 JSON.stringify(data),
                {headers: this.headers}
            ).map((res:any) =>res.json());
        }
    }

    removeNode(nodeId: string){
        return this.http.delete(
            this.postPutConversationApi + "/"+nodeId,
            {headers: this.headers}
        ).map((res:any) =>res.json());
    }
}