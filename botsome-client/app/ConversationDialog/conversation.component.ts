import { Component, OnInit } from "@angular/core";
import { Observable } from 'rxjs/Rx';

import { ConversationNode } from "../Models/conversationNode";
import { ConversationService } from "./conversation.service";
import { IntentService } from "../Intents/intents.service";
import { EntityService } from "../Entities/entities.service";
import { Intent } from "../Models/intent";
import { Entity } from "../Models/entity";

@Component({
    selector: "conversation-dialog",
    templateUrl: "app/ConversationDialog/conversation.component.html"
})
export class ConversationComponent implements OnInit{
    idsToNodesMap: {};
    errorMessages: string[];
    saveResults: {
        successfulUpdates: number,
        failedUpdates: number,
        successfulDeletes: number,
        failedDeletes: number
    };
    conversationRoot = null;
    editMode: Boolean;
    conversationCopy: ConversationNode;
    newId:number = 0;
    idsToNodesMapCopy: {};

    constructor(private conversationService: ConversationService,
                private intentService: IntentService, 
                private entityService: EntityService){
        this.errorMessages = [];
        this.editMode = false;
        this.saveResults = null;
    }

    ngOnInit(){
        this.loadConversationNodes();
    }

    resetSaveResults(){
        this.saveResults = {
            successfulUpdates: 0,
            failedUpdates: 0,
            successfulDeletes: 0,
            failedDeletes: 0
        };
    }

    removeSuccessAlert(){
        this.saveResults = null;
    }

    removeAlert(){
        this.errorMessages = null;
    }

    loadConversationNodes(){
        this.conversationService.getAllNodes()
        .subscribe((nodes) => {
            var intentsObserv = this.intentService.getIntents();
            var entitiesObserv = this.entityService.getEntities();
            Observable.forkJoin(intentsObserv, entitiesObserv)
            .subscribe((results: any[]) =>{
                var intents: Intent[]= results[0];
                var entities: Entity[]= results[1];

                var mappedNodes = nodes.map( (node)=> {
                    if(node.type == "entity"){
                        var entity = entities.filter(x=> {return x._id == node.result_id})[0];
                        if(entity){
                            node.associatedEntity = {
                                _id : entity._id,
                                bot_id: entity.bot_id,
                                entity_id: entity.entity_id,
                                is_value_restricted: entity.is_value_restricted,
                                name: entity.name,
                                values: entity.values
                            };
                        }
                    }else if(node.type == "intent"){
                        var intent = intents.filter(x => {return x._id == node.result_id})[0];
                        if(intent){
                            node.associatedIntent = {
                                _id: intent._id,
                                bot_id: intent.bot_id,
                                created_date: intent.created_date,
                                intent_id: intent.intent_id,
                                name: intent.name
                            };
                        }
                    }
                    return node;
                });
                var structureResult = this.generateTreeStructure(mappedNodes);
                this.conversationRoot = structureResult.root;
                this.idsToNodesMap = structureResult.idsMap; 
            })
        },
        (err) => {
            this.errorMessages.push(err);
        })
    }

    generateTreeStructure(node: ConversationNode[]){
        var conversationRoot: ConversationNode;
        var idsToNodesMap: {} = {};

        node.forEach(function(node:ConversationNode){
            //each node will have children, so let's give it a "children" poperty
            node.childNodes = [];

            //add an entry for this node to the map so that any future children can
            //lookup the parent

            //Does this node have a parent?
            if(node.parent_node_id == "0") {
                //Doesn't look like it, so this node is the root of the tree
                node.treeLevel = 0;
                conversationRoot = node;        
            } else {        
                //This node has a parent, so let's look it up using the id
                var parentNode = idsToNodesMap[node.parent_node_id];
                //Let's add the current node as a child of the parent node.
                node.treeLevel = parentNode.treeLevel + 1;  
       
                if(parentNode.childNodes.length == 0){                     
                    node.canBeEntityType = true;
                }else if(parentNode.childNodes.length == 1){
                    parentNode.childNodes[0].canBeEntityType = false;
                    node.canBeEntityType = false;
                }else{
                    node.canBeEntityType = false;
                }
                
                parentNode.childNodes.push(node); 
            }
            idsToNodesMap[node._id] = node;
        });
        return {
            idsMap : idsToNodesMap,
            root: conversationRoot
        }
    }

    editConversation(){
        this.editMode = true;
        this.conversationCopy = this.conversationRoot;
        this.idsToNodesMapCopy = this.idsToNodesMap;
    }

    cancelEdit(){
        this.conversationRoot = this.conversationCopy;
        this.idsToNodesMap = this.idsToNodesMapCopy;
        this.conversationCopy = null;
        this.idsToNodesMapCopy = null;
        this.editMode = false;
    }

    saveConversation(){
        this.errorMessages = [];

        //validate the data
        this.validateData(this.conversationRoot);
        if(this.errorMessages.length != 0){
            return;
        }
        this.resetSaveResults();
        this.saveNodes(this.conversationRoot);
        this.refreshConversationTree(this.conversationRoot);
        this.editMode = false;
    }

    private saveNodes(node: ConversationNode){
        if(node.markedForDeletion && node._id >= "0"){
            this.conversationService.removeNode(node._id)
            .subscribe((x) => {this.saveResults.successfulDeletes++;},
                       (err) => {this.saveResults.failedDeletes++;});
        }

        else if(!node.markedForDeletion){
            this.conversationService.updateNode(node)
            .subscribe((resp) => {
                    this.saveResults.successfulUpdates++;
                    if(resp.newId){
                        //update ids
                        node.childNodes.forEach((node) => {
                            node.parent_node_id = resp.newId;
                        });
                        node._id = resp.newId;
                    }
                },
                (err) => {this.saveResults.failedUpdates++;});
        }

        node.childNodes.forEach((childNode) =>{
            this.saveNodes(childNode);
        })
    }

    private refreshConversationTree(node: ConversationNode){
        if(node.markedForDeletion){            
            this.idsToNodesMap[node._id] = null;
            var parentNode = this.idsToNodesMap[node.parent_node_id];
            if(parentNode){
                var index = parentNode.childNodes.indexOf(node);
                if(index >= 0){
                    parentNode.childNodes.splice(index, 1);
                }
            }
        }
        node.childNodes.forEach((childNode)=>{
            this.refreshConversationTree(childNode);
        })
    }

    private validateData(node: ConversationNode){
        if(node.type == "entity" && (node.associatedEntity == null 
            || node.associatedEntity == undefined) &&
            node.parent_node_id != '0' && !node.markedForDeletion) {
            this.errorMessages.push("Nodes with type entity need to have an associated entity entry.");
            return;
        }
        if(node.type == "intent" && (node.associatedIntent == null ||
            node.associatedIntent == undefined) && 
            node.parent_node_id != "0" && !node.markedForDeletion){
            this.errorMessages.push("Nodes with type intent need to have an associated intent entry.");
            return;
        }
        if(node.type != "intent" && node.type != "entity" && !node.markedForDeletion){
            this.errorMessages.push("Node type is a required field.");
            return;
        }

        if(node.parent_node_id != "0" && !node.markedForDeletion && node.response.trim() == ""){
            this.errorMessages.push("Bot response is required in a dialog node.");
            return;
        }
        if(node.type == "entity"){
            var parentNode = this.idsToNodesMap[node.parent_node_id];
            parentNode.waits_entity_response = node.result_id;
        }
        node.childNodes.forEach((childNode) => {
            this.validateData(childNode);
        })
    }

    onSomeNodeAdded($event){
        this.newId = this.newId - 1;
        $event[0]._id = this.newId;
        this.idsToNodesMap[$event[0]._id] = $event[0];
    }

    onSomeNodeRemoved($event){
        var node = this.idsToNodesMap[$event[0]];
        node.markedForDeletion = true;
        node.childNodes.forEach((x) => {
            x.markedForDeletion = true;
        })
    }
}