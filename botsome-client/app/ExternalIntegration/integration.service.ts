import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Constants } from '../app.constants';
import { Integration } from '../Models/integration';

@Injectable()
export class IntegrationService {
    getIntegrationsApiUrl = 
        Constants.ADMIN_SERVER_BASE + "/api/" + Constants.CURRENT_BOT_ID + "/integrations";
    integrationsApiUrl = Constants.ADMIN_SERVER_BASE + "/api/integrations";

    headers = new Headers({
        'Content-Type': 'application/json'
    });

    constructor (private http: Http) {}

      getIntegrations() : Observable<Integration[]> {

         // ...using get request
         return this.http.get(this.getIntegrationsApiUrl)
                        // ...and calling .json() on the response to return data
                         .map((res:Response) => res.json())
                         //...errors if any
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

     }

     updateIntegration(integration: Integration): Observable<Response>{
    
         if(integration._id == "0"){
          return this.http.post(
             this.integrationsApiUrl,
             JSON.stringify(integration),
             {headers: this.headers}
            ).map((res: any) => res.json());
         }else{
             return this.http.put(
             this.integrationsApiUrl + "/" + integration._id,
             JSON.stringify(integration),
             {headers: this.headers}
            ).map((res: any) => res.json());
         }
     }

     removeIntegration(integrationId: string): Observable<Response>{
         return this.http.delete(
           this.integrationsApiUrl + "/" + integrationId,
             {headers: this.headers}
            ).map((res: any) => res.json());
     }
}