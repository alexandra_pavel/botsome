import { Component, OnInit } from "@angular/core";
import { FormsModule } from '@angular/forms';
import { IntegrationService } from "./integration.service";
import { Integration } from "../Models/integration";
import { Constants } from "../app.constants";

@Component({
    selector: "integrations",
    templateUrl: "app/ExternalIntegration/integration.component.html"
})
export class IntegrationComponent implements OnInit{
    integrations: Integration[];
    integrationsCopy: Integration[];
    errorMessages: string[];
    successfulMsg: string;

    constructor(private IntegrationService: IntegrationService){
        this.errorMessages = [];
        this.integrationsCopy = [];
        this.successfulMsg = null;
    }

    removeAlert(i:number){
        this.errorMessages.splice(i,1);
    }

    removeSuccessAlert(){
        this.successfulMsg = null;
    }

    ngOnInit(){
        this.loadIntegrations();
    }

    loadIntegrations(){
        this.IntegrationService.getIntegrations()
        .subscribe((integrations) => {
            this.integrations = integrations;
        }, (err) => {
            this.errorMessages.push(err);
        });
    }

    addNewIntegration(){
        var integration = new Integration();
        integration._id = "0";
        integration.isInEditMode = true;
        integration.bot_id = Constants.CURRENT_BOT_ID;
        integration.type = "post";
        this.integrations.unshift(integration);
    }

    editIntegration(integration: Integration){
        this.integrationsCopy.push(integration);
        integration.isInEditMode = true;
    }

    saveIntegrationChanges(integration: Integration){
        this.IntegrationService.updateIntegration(integration)
        .subscribe((res: any) => {
            if(res.newId){
                integration._id = res.newId;
            }else{
                //remove from temporary copies
                var integrationCopy = this.integrationsCopy.filter(x=> {return x._id == integration._id})[0];
                var index = this.integrationsCopy.indexOf(integrationCopy);
                if(index >= 0){
                    this.integrationsCopy.splice(index, 1);
                }
            }
            this.successfulMsg = "Integration "+ integration.name +  " updated successfully";
            integration.isInEditMode = false;
        },
        (err) => {
            this.errorMessages.push(err);
        });
    }

    removeIntegration(integration: Integration){
        this.IntegrationService.removeIntegration(integration._id)
        .subscribe(
            (res) =>{
                var index = this.integrations.indexOf(integration);
                if(index >= 0){
                    this.integrations.splice(index, 1);
                }
                this.successfulMsg = "Entity " + integration.name + " was successfully removed.";
            },
        (err) => {this.errorMessages.push(err);}
        )
    }

    closeEditIntegration(integration: Integration){
        if(integration._id == "0"){
            let index = this.integrations.indexOf(integration);
            if(index >= 0){
                this.integrations.splice(index,1);
            }
        }else{
            var integrationCopy = this.integrationsCopy.filter(x => {return x._id == integration._id})[0];
            let index = this.integrationsCopy.indexOf(integrationCopy);
            if(index >= 0){
                this.integrationsCopy.splice(index, 1);
            }
        }
        integration.isInEditMode = false;
    }

}