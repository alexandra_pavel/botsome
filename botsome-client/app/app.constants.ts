export class Constants {  
    public static ADMIN_SERVER_BASE: string = "http://localhost:3000";
    public static CHAT_SERVER_POST_URL: string = "http://localhost:5000";
    public static CURRENT_BOT_ID: number = 2;
}