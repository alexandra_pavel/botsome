import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Constants } from '../app.constants';
import { Intent } from '../Models/intent';
import { IntentModel } from '../Models/intent';
import { IntentQuestion } from '../Models/intentQuestion';

@Injectable()
export class IntentService {
    getIntentsApiUrl = 
        Constants.ADMIN_SERVER_BASE + "/api/" + Constants.CURRENT_BOT_ID + "/intents";
    getQuestionsApiUrl = Constants.ADMIN_SERVER_BASE + "/api/questions";

    intentApiUrl = Constants.ADMIN_SERVER_BASE + "/api/intents";
    headers = new Headers({
        'Content-Type': 'application/json'
    });

    constructor (private http: Http) {}

      getIntents() : Observable<Intent[]> {
        let intentsUrl = this.getIntentsApiUrl;

         // ...using get request
         return this.http.get(intentsUrl)
                        // ...and calling .json() on the response to return data
                         .map((res:Response) => res.json())
                         //...errors if any
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

     }

     getIntentQuestions(): Observable<IntentQuestion[]> {
         let questionsUrl =this.getQuestionsApiUrl;

          // ...using get request
         return this.http.get(questionsUrl)
                        // ...and calling .json() on the response to return data
                         .map((res:Response) => res.json())
                         //...errors if any
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
     }

     updateIntent(intent: Intent): Observable<Response>{
         var data = new IntentModel();
         data._id = intent._id;
         data.bot_id = intent.bot_id;
         data.created_date = intent.created_date;
         data.name = intent.name;
         data.intent_id = intent.intent_id;

         if(data._id == "0"){
             return this.http.post(
                 this.intentApiUrl,
                 JSON.stringify(data),
                {headers: this.headers}
             ).map((res:any) =>res.json());
         }
         else{
            return this.http.put(
                this.intentApiUrl + "/" + data._id,
                JSON.stringify(data),
                {headers: this.headers}
            ).map((res:any) =>res.json());
         }
     }

     updateQuestion(question: IntentQuestion): Observable<Response>{
          return this.http.put(
             this.getQuestionsApiUrl +"/"+question._id,
             JSON.stringify(question),
             {headers: this.headers}
            ).map((res: any) => res.json());
     }

     deleteQuestion(questionId: string): Observable<Response>{
         return this.http.delete(
           this.getQuestionsApiUrl +"/"+questionId,
             {headers: this.headers}
            ).map((res: any) => res.json());
     }

     deleteIntent(intentId: string): Observable<Response>{
         return this.http.delete(
             this.intentApiUrl + "/"+intentId,
             {headers: this.headers}
         ).map((res:any) =>res.json());
     }
}