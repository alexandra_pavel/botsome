import { Component, 
         OnInit,
         ViewChild,
         ElementRef,
         AfterViewInit,
         NgZone } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { FormsModule } from '@angular/forms';
import { ChangeDetectorRef } from '@angular/core';

import { IntentService } from "./intents.service";
import { Intent } from "../Models/intent";
import { IntentQuestion } from "../Models/intentQuestion";
import { Constants } from "../app.constants";

declare var $: any;

@Component({
    selector: "intents",
    templateUrl: "app/Intents/intents.component.html"
})
export class IntentsComponent implements OnInit {
    intents: Intent[];
    questions: IntentQuestion[];
    errorMessages: string[];
    intentsCopy: Intent[];
    successMsg: string;

    constructor(private intentService: IntentService,
                private ref: ChangeDetectorRef,
                private zone:NgZone){
        this.intentsCopy = [];
        this.errorMessages = [];        
        this.successMsg = null;
    }

    removeAlert(i:number){
        this.errorMessages.splice(i,1);
    }

    removeSuccessAlert(){
        this.successMsg = null;
    }

    ngOnInit(){
        this.loadIntents();
    }

    loadIntents(){    
         this.intentService.getIntents()
        .subscribe(
            intents => {
                this.intents = intents; //Bind to view
                this.loadQuestions();                
            },
            err => {
                // Log errors if any
                console.log(err);
                this.errorMessages.push(err);
            });
    }

    private loadQuestions(){
        this.intentService.getIntentQuestions()
        .subscribe(
            questions => {
                this.questions = questions;
                for(var intent of this.intents){
                    intent.questions = this.questions.filter( x=> x.intent_obj_id == intent._id);
                }
            },
            err => {
                // Log errors if any
                console.log(err);
                this.errorMessages.push(err);
            }
        )
    }

    editIntent = (intent: Intent) => {
        if(!intent.isInEditMode){  
                intent.isInEditMode = true;
        }
        this.intentsCopy.push(intent);
    }

    addNewIntent(){
        var existingNewIntents = this.intents.filter(i => {return i._id == "0"});
        if(existingNewIntents.length !=0){
            this.errorMessages.push("Please edit the intent previously added.");
            return;
        }

        var intent = new Intent();
        intent._id = "0";
        intent.bot_id = Constants.CURRENT_BOT_ID;
        intent.isInEditMode = true;
        intent.questions = [];
        this.intents.unshift(intent);
    }

    saveIntentChanges(intent: Intent){
        var successfulResult = true;
        this.intentService.updateIntent(intent)
        .subscribe((response: any) =>{
            //start updating the questions.
            if(response.newId){
                intent._id = response.newId;
            }
            var obsQuestion = [];
            for(var question of intent.questions){
                question.intent_obj_id = intent._id;
                if(!question.marked_for_deletion){
                    obsQuestion.push(this.intentService.updateQuestion(question)
                        .map(
                            (res: any) => {
                                console.log(res);
                                if(res.newId){
                                    question._id = res.newId;
                                }
                                return true;
                            }, 
                            (err)=> {
                                return false;
                            }));
                }else{
                    obsQuestion.push(this.intentService.deleteQuestion(question._id)
                        .map(
                            (res: any) => {
                                console.log(res);
                                if(res.newId){
                                    question._id = res.newId;
                                }
                                return true;
                            }, 
                            (err)=> {
                                return false;
                            }));
                }
            }
            if(obsQuestion.length == 0){
                intent.isInEditMode = false;
            }
            else{
                Observable.forkJoin(obsQuestion)
                .subscribe((res:any[]) =>{
                        var successfulResult = res.filter(x => {return x != true}).length == 0;

                        if(successfulResult){
                            var intentCopy = this.intentsCopy.filter(x=> {return x._id == intent._id})[0];
                            var index = this.intentsCopy.indexOf(intentCopy);
                            if(index >= 0){
                                this.intentsCopy.splice(index, 1);
                            }
                            this.successMsg = "Intent " + intent.name + " was successfully saved";
                            intent.isInEditMode = false;
                        }
                });
            }
        },
        (err) => {
            this.errorMessages.push(err); 
        });
        
    }
    

    removeIntent(intent: Intent){
        var successfulResult = true;

         this.intentService.deleteIntent(intent._id)
        .subscribe((response: any) =>{
            var obsQuest:Observable<any>[] = [];

            for(var question of intent.questions){
                obsQuest.push(this.intentService.deleteQuestion(question._id)
                    .map((res) =>{
                        return true;
                    }, (err) => {
                        this.errorMessages.push(err);
                        return false;
                    }));
            }
            if(obsQuest.length == 0){
                 var index = this.intents.indexOf(intent);
                    if(index >= 0){
                        this.intents.splice(index, 1);
                    }
                    this.successMsg = "Intent "+ intent.name + " was successfully removed.";
            }
            Observable.forkJoin(obsQuest)
            .subscribe((res: any[]) => {
                if(res.filter(x => {return x != true}).length != 0){
                    successfulResult = false;
                }
                if(successfulResult){
                    var index = this.intents.indexOf(intent);
                    if(index >= 0){
                        this.intents.splice(index, 1);
                    }
                    this.successMsg = "Intent "+ intent.name + " was successfully removed.";
                }
            })
        },
        (err) => {
            this.errorMessages.push(err);
        });
    }

    deleteQuestion(question: IntentQuestion,  intent: Intent){
        if(question._id == "0"){
            this.removeQuestion(question._id, question.question, intent.questions);
        }
        this.intentService.deleteQuestion(question._id)
        .subscribe((res)=> {
            this.successMsg = 'The question was successfully deleted.';
            var intentCopy = this.intentsCopy.filter(x=> {return x.questions.filter(q=> q._id == question._id).length != 0})[0];
            this.removeQuestion(question._id, question.question, intent.questions);
            this.removeQuestion(question._id, question.question, intentCopy.questions);
            
        }, (err) => {
            this.errorMessages.push(err.json());
        })
    }

    private removeQuestion(questionId: string, name:string, questions: IntentQuestion[]){
        if(questionId == "0"){
            var ques = questions.filter(q => {return q._id == questionId && q.question == name})[0];
            var qI = questions.indexOf(ques, 0);
            if(qI>=0){
                questions.splice(qI, 1);
            }
        }else{
             var ques = questions.filter(q => {return q._id == questionId && q.question == name})[0];
             ques.marked_for_deletion = true;
        }
    }

    addNewQuestion(intent: Intent){
        var existingNewQuestions = intent.questions.filter(x => {return x._id == "0" && x.question == ""});
        if(existingNewQuestions.length == 0){
            var question = new IntentQuestion();
            question._id = "0";
            question.intent_id = intent.intent_id;
            question.intent_obj_id = intent._id;
            question.question = "";

            intent.questions.push(question);
        }
        else{
            this.errorMessages.push("Please edit the question you previously added.");
        }
    }

    closeEditIntent(intent: Intent){
        var intentCopy = this.intentsCopy.filter(x=> {return x._id == intent._id})[0];
        if(intentCopy){
            var index = this.intentsCopy.indexOf(intentCopy);
            intentCopy.isInEditMode = false;
            intent = intentCopy;
            this.intentsCopy.splice(index, 1);
        }
        if(intent._id == "0"){
            var index = this.intents.indexOf(intent);
            if(index>=0){
                this.intents.splice(index, 1);
            }
        }
    }
}