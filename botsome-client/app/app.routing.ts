import { NgModule }                 from "@angular/core";
import { Routes, RouterModule }     from "@angular/router";

import { AppComponent }             from "./app.component";
import { DashboardComponent }       from './Dashboard/dashboard.component';
import { HomeComponent }            from './Home/home.component';
import { IntentsComponent }         from './Intents/intents.component';
import { EntitiesComponent }        from './Entities/entities.component';
import { ConversationComponent }    from './ConversationDialog/conversation.component';
import { ChatComponent }            from './Chat/chat.component';
import { IntegrationComponent }       from './ExternalIntegration/integration.component';

const appRoutes: Routes = [
    { path: "", component: HomeComponent },
    { path: "home", component: HomeComponent } ,
    { path: "intents", component: IntentsComponent },
    { path: "entities", component: EntitiesComponent },
    { path: "dialog", component: ConversationComponent },
    { path: "dashboard", component: DashboardComponent },
    { path: "chat", component: ChatComponent },
    { path: "integrations", component: IntegrationComponent },
    { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ],
  providers: [
  ]
})
export class AppRoutingModule { }

