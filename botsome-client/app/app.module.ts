import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule }     from '@angular/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AppRoutingModule } from "./app.routing";
import { Router } from '@angular/router';
import { ChartModule } from 'angular2-chartjs';

//Components
import { AppComponent }  from './app.component';
import { DashboardComponent } from './Dashboard/dashboard.component';
import { HomeComponent } from './Home/home.component';
import { IntentsComponent } from './Intents/intents.component';
import { EntitiesComponent } from './Entities/entities.component';
import { ConversationComponent } from './ConversationDialog/conversation.component';
import { ChatComponent } from './Chat/chat.component';
import { IntegrationComponent } from './ExternalIntegration/integration.component';

//Services
import { IntentService } from './Intents/intents.service';
import { EntityService } from './Entities/entities.service';
import { ConversationService } from './ConversationDialog/conversation.service';
import { ConversationNodeComponent } from './ConversationDialog/node.component';
import { ChatService } from './Chat/chat.service';
import { IntegrationService } from './ExternalIntegration/integration.service';

@NgModule({
  imports: [ 
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ChartModule
  ],
  declarations: [ 
      AppComponent,
      DashboardComponent,
      HomeComponent,
      IntentsComponent,
      EntitiesComponent,
      ConversationComponent,
      ConversationNodeComponent,
      ChatComponent,
      IntegrationComponent
   ], 
  providers: [ 
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        IntentService,
        EntityService,
        ConversationService,
        ChatService,
        IntegrationService
   ],
  bootstrap:    [ AppComponent ],
})
export class AppModule { 
}