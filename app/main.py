from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin

import sys
import json
import requests

# Add the ptdraft folder path to the sys.path list
sys.path.append('/data-mgmt/')
from data_mgmt.base_data_retriever import BaseDataRetriever
from data_mgmt.chatbot_admin_repository import ChatbotAdminRepository
from conversation import ConversationDialog
from intent_classification import IntentClassifier
from conversation_tree import Tree
from app_constants import Constants

app = Flask(__name__)
CORS(app)

admin_repository = ChatbotAdminRepository()
bot_data_retriever = BaseDataRetriever()
conversation_dialogs = {}
bots = {}
bot_id = 2
fb_bot_access_token = ''


# Facebook verification method
@app.route('/fb_messenger', methods=['GET'])
def fb_verify():
    # our endpoint echos back the 'hub.challenge' value specified when we setup the webhook
    if request.args.get("hub.mode") == "subscribe" and request.args.get("hub.challenge"):
        if not request.args.get("hub.verify_token") == bots[bot_id]["verify_token"]:
            return "Verification token mismatch", 403
        return request.args["hub.challenge"], 200

    return 'Hello World (from Botsome!)', 200


@app.route('/', methods=['GET'])
def echo():
    return 'Echo', 200


def reply(user_id, msg):
    data = {
        "recipient": {"id": user_id},
        "message": {"text": msg}
    }
    resp = requests.post("https://graph.facebook.com/v2.6/me/messages?access_token=" + bots[bot_id]["fb_access_token"],
                         json=data)
    print(resp.content)


def send_to_history(sender, recipient, message, node_result_id):
    message = {
        'sender': sender,
        'recipient': recipient,
        'message': message,
        'bot_id': bot_id,
        'node_result_id': node_result_id
    };

    resp = requests.post(Constants.ADMIN_API, json=message)

@app.route('/fb_messenger', methods=['POST'])
def handle_fb_incoming_messages():
    data = request.json
    sender = data['entry'][0]['messaging'][0]['sender']['id']
    message = data['entry'][0]['messaging'][0]['message']['text']

    send_to_history(sender=sender, recipient='chatbot', message=message, node_result_id='0')

    conv_dialog = get_conversation_dialog(bot_id=bot_id, user_id=sender)

    # try get new conversation response only if the message doesn't repeat itself.
    new_message_was_set = conv_dialog.set_new_message(message=message)
    if new_message_was_set is True:
        send_to_history(sender=sender, recipient='chatbot', message=message, node_result_id='0')

        output = conv_dialog.get_response(user_input=message)
        send_to_history(sender='chatbot', recipient=sender,
                        message=output['response_message'], node_result_id=output['node_result_id'])
        reply(sender, output['response_message'])

    message = {'message': "ok"}
    response = jsonify(message)
    response.status_code = 200
    return response


@app.route('/api/chat_messenger', methods=["POST"])
@cross_origin()
def handle_chat_incoming_messages():
    data = request.json
    print(data)
    message = data['message']

    conv_dialog = get_conversation_dialog(bot_id=bot_id, user_id='test_chat')
    output = {
        'response_message': '',
        'node_result_id': '0',
        'node_type': '0'
    }
    # try get new conversation response only if the message doesn't repeat itself.
    new_message_was_set = conv_dialog.set_new_message(message=message)
    if new_message_was_set is True:
        output = conv_dialog.get_response(user_input=message)
    print(output)

    response = jsonify(output)
    response.status_code = 200
    return response


# noinspection PyBroadException
@app.route('/api/reset_chatbot', methods=["POST"])
def reset_chatbot():
    try:
        initialize_bot(bot_id=bot_id, override_existing=True)
        get_conversation_dialog(bot_id=bot_id, user_id="test_chat", override_existing=True)
        response = jsonify("Bot was reset.")
        response.status_code = 200
    except:
        print("Unexpected error: ", sys.exc_info()[0])
        response = jsonify(sys.exc_info()[0])
        response.status_code = 500

    return response


def initialize_bot(bot_id, override_existing=False):
    if bot_id not in bots or override_existing:
        new_bot = admin_repository.get_chat_bot(bot_id=bot_id)
        new_bot['classifier'] = IntentClassifier(data_retriever=bot_data_retriever, bot_id=bot_id,
                                                 classifier=Constants.CLASSIFIER)
        new_bot['tree'] = Tree(data_retriever=bot_data_retriever, bot_id=bot_id)
        bots[bot_id] = new_bot
    return bots[bot_id]


def get_conversation_dialog(bot_id, user_id, override_existing=False):
    dialog_key = str(bot_id) + user_id
    if dialog_key in conversation_dialogs:
        if override_existing:
            new_dialog = ConversationDialog(data_retriever=bot_data_retriever,
                                            intent_classifier=bots[bot_id]['classifier'],
                                            conversation_tree=bots[bot_id]['tree'])
            conversation_dialogs[dialog_key] = new_dialog

        return conversation_dialogs[dialog_key]
    else:
        new_dialog = ConversationDialog(data_retriever=bot_data_retriever, intent_classifier=bots[bot_id]['classifier'],
                                        conversation_tree=bots[bot_id]['tree'])
        conversation_dialogs[dialog_key] = new_dialog
        return new_dialog


if __name__ == "__main__":
    initialize_bot(bot_id=bot_id)
    # statistics = bots[bot_id]['classifier'].test_classifiers()
    # print(statistics)
    app.run()
