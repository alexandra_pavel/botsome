from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.linear_model import Perceptron
from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
import numpy as np
import csv


class IntentClassifier:
    data = []
    intents = []
    intents_dic = {}
    all_intents = []

    def __init__(self, data_retriever, classifier='NB Multinomial', bot_id=2):
        # init training data
        self.data, self.intents, self.intents_dic = data_retriever.get_training_dat(bot_id=bot_id)

        unique = np.unique(self.intents)
        self.all_intents = np.array(range(len(unique)))

        # init classifier and text transform
        self.vectorizer = HashingVectorizer(decode_error='ignore', n_features=2 ** 18,
                                            non_negative=True)

        self.classifiers = {
            'Perceptron': Perceptron(),
            'NB Multinomial': MultinomialNB(alpha=0.01),  # ignores non-occuring features
            'KNeighbors': KNeighborsClassifier(n_neighbors=10)
        }

        # Train classifiers with train data
        x_train = self.vectorizer.transform(self.data)

        for cls_name, cls in self.classifiers.items():
            # update classifier with the train data
            cls.fit(x_train, self.intents)

        self.classifier = self.classifiers[classifier]

    def get_intent(self, input_question=''):
        # convert the test data into features
        test_data = [input_question]
        x_test = self.vectorizer.transform(test_data)
        result = self.classifier.predict(x_test)
        return {
            'intent': result[0],
            '_id': self.intents_dic[result[0]]
        }

    @staticmethod
    def read_from_csv(file_name=''):
        data = []
        intents = []

        with open(file_name, newline='') as csv_file:
            data_reader = csv.reader(csv_file, delimiter=',', quotechar='"')
            rows_list = []

            for row in data_reader:
                rows_list.append(row)

            for item in rows_list:
                intent_name = item[1]
                question = item[2]
                data.append(question)
                intents.append(intent_name)

        return data, intents

    def test_classifiers(self, test_data_source='csv'):
        test_data = []
        test_intents = []

        if test_data_source == 'csv':
            test_data, test_intents = self.read_from_csv('data/test-data.csv')
        x_test = self.vectorizer.transform(test_data)

        cls_stats = {}

        for cls_name, cls in self.classifiers.items():
            # accumulate test accuracy stats
            stats = {
                'accuracy': cls.score(x_test, test_intents)
            }
            cls_stats[cls_name] = stats

        return cls_stats
