class Constants(object):
    MONGO_CONNECTION = 'localhost:27017'

    MONGO_DB_NAME = 'botsomedb'

    # csv or mongo
    DATA_SOURCE = 'mongo'

    CLASSIFIER = 'Perceptron'

    ADMIN_API = 'http://localhost:3000/api/chatMessages'
