import string
import json
import requests
from collections import namedtuple


class ConversationDialog:
    context = {}  # here we'll store the entities
    intents = {}
    entities = {}
    answer = ''

    def __init__(self, data_retriever, bot_id=2, intent_classifier=None, conversation_tree=None):
        self.data_retriever = data_retriever
        self.bot_id = bot_id
        self.intent_classifier = intent_classifier
        self.tree = conversation_tree
        self.intents = self.intent_classifier.intents_dic
        self.entities = self.data_retriever.get_entities(bot_id=bot_id)
        self.current_node = self.tree.tree
        self.context['last_message'] = ''

    def get_response(self, user_input):
        next_node = None

        if self.current_node.waits_entity != '0':
            next_node = self.handle_entity_required_node(user_input=user_input)
        else:
            next_node = self.search_intent_node(user_input=user_input)

        if next_node is None:
            if self.answer != '':
                response = self.answer
                self.answer = ''
                return {
                    'response_message': response,
                    'node_result_id': 'unidentified',
                    'node_type': 'unidentified',
                    'result_type': 'chatbot'
                }
            else:
                return {
                    'response_message': 'Sorry. I did not find anything for your request: ' + user_input,
                    'node_result_id': 'unidentified',
                    'node_type': 'unidentified',
                    'result_type': 'chatbot'
                }
        else:
            if next_node.response_type == 'text':
                self.current_node = next_node
                return {
                    'response_message': self.get_parsed_response(self.current_node.response),
                    'node_result_id': self.current_node.result_id,
                    'node_type': self.current_node.node_type,
                    'result_type': 'chatbot'
                }
            elif next_node.response_type == 'external':
                self.current_node = next_node

                return {
                    'response_message': self.handle_external_api_text(next_node=self.current_node),
                    'node_result_id': self.current_node.result_id,
                    'node_type': self.current_node.node_type,
                    'result_type': 'external api'
                }

    def set_new_message(self, message):
        if self.context['last_message'] != message:
            self.context['last_message'] = message
            return True
        else:
            return False

    def get_parsed_response(self, rsp=''):
        exclude = set(string.punctuation)
        exclude.remove('@')
        exclude.remove('_')
        exclude
        s = ''.join(ch for ch in rsp if ch not in exclude)

        words = s.split(' ')

        for word in words:
            if word.find('@') == 0:
                entity_name = word[1:]
                rsp = rsp.replace(word, self.context[entity_name])

        return rsp

    def search_intent_node(self, user_input):
        intent_prediction = self.intent_classifier.get_intent(input_question=user_input)
        parent_node = self.current_node
        next_node = None

        while next_node is None and parent_node is not None:
            if len(parent_node.child_nodes) == 0:
                parent_node = self.tree.get_tree_node_by_node_id(parent_node.parent_node_id)
                if parent_node is not None:
                    next_node = self.tree.get_next_node(intent_prediction['_id'], parent_node)
            else:
                next_node = self.tree.get_next_node(intent_prediction['_id'], parent_node)
                if next_node is None:
                    # none of my children is the intent I am looking for, move to parent node:
                    parent_node = self.tree.get_tree_node_by_node_id(parent_node.parent_node_id)

        return next_node

    def handle_entity_required_node(self, user_input):
        entity_id = self.current_node.waits_entity
        entity = self.data_retriever.get_entity(entity_id=entity_id)
        is_valid_entity = False

        if entity is not None and entity['is_value_restricted']:
            values = [x.strip() for x in entity['values'].split(";")]
            if user_input in values:
                is_valid_entity = True
            else:
                is_valid_entity = False
        elif entity is not None and entity['is_value_restricted'] is False:
            is_valid_entity = True

        if is_valid_entity is True:
            next_node = self.tree.get_next_node(self.current_node.waits_entity, self.current_node)
            if next_node is not None:
                entity_name = self.entities[entity_id]['name']
                next_node.response = next_node.response.replace('@' + entity_name, user_input)
                self.context[entity_name] = user_input
            return next_node
        else:
            # check if the input is maye another intent, if the value is not valid
            next_node = self.search_intent_node(user_input=user_input)
            if next_node is not None:
                return next_node

            # if is not another intent and invalid value, say the value is invalid
            self.answer = entity['name'] + " value: '" + user_input + "' is invalid."
            return None

        return None

    # noinspection PyBroadException
    def handle_external_api_text(self, next_node):
        external_entry = self.data_retriever.get_external_source(source_id=next_node.response_msg_id)
        if external_entry:
            post_message = self.get_parsed_response(external_entry['post_message'])
            post_data = json.loads(post_message, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))

            try:
                resp = requests.post(
                    external_entry['external_post_api_url'],
                    json=post_data)
                response = self.get_parsed_response(next_node.response).replace('{external_placeholder}', resp.text)
                return response
            except:
                return 'We are sorry for the inconvenience. Our service seems to be down.'
        else:
            return 'Please provide an external api for this node.'

