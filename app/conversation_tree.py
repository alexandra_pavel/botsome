from node import Node


class Tree:
    nodes_list = []

    def __init__(self, data_retriever, bot_id=2):
        self.nodes_list = data_retriever.get_tree(bot_id=bot_id)
        root_node_id = self.nodes_list[0]['_id']
        self.tree = self.build_tree(node_id=root_node_id)

    def get_tree_node_by_node_id(self, node_id, root=None):
        if root is None:
            root = self.tree

        if root._id == node_id:
            return root

        if len(root.child_nodes) > 0:
            for child in root.child_nodes:
                node = self.get_tree_node_by_node_id(node_id, child)
                if node is not None:
                    return node
        else:
            return None

    @staticmethod
    def get_next_node(intent_id, current_node):
        for node in current_node.child_nodes:
            if node.result_id == intent_id:
                return node
        return None

    def build_tree(self, node_id, parent_node_id='0'):
        current_item = {}
        child_nodes = []
        for item in self.nodes_list:
            if item['parent_node_id'] == node_id:
                child_nodes.append(item)
            if item['_id'] == node_id:
                current_item = item

        child_subtrees = []
        for child in child_nodes:
            child_subtrees.append(self.build_tree(child['_id'], current_item['_id']))

        return Node(_id=current_item['_id'],
                    node_type=current_item['node_type'],
                    response=current_item['response'],
                    result_id=current_item['result_id'],
                    waits_entity=current_item['waits_entity'],
                    response_msg_id=current_item['response_msg_id'],
                    response_type=current_item['response_type'],
                    child_nodes=child_subtrees,
                    parent_node_id=parent_node_id)

