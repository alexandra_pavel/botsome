from pymongo import MongoClient
from bson import ObjectId
from app_constants import Constants


class MongoDataRetriever:
    def __init__(self):
        client = MongoClient(Constants.MONGO_CONNECTION)
        self.db = client.botsomedb

    def get_intents(self, bot_id=2):
        intents_cursor = self.db.intent.aggregate([
            {
                "$lookup":
                    {
                        "from": "intent_question",
                        "localField": "_id",
                        "foreignField": "intent_obj_id",
                        "as": "questions"
                    }
            },
            {
                "$match":
                    {
                        "bot_id": bot_id
                    }
            }
        ])
        return list(intents_cursor)

    def get_training_data(self, bot_id=2):
        data = []
        intents = []
        intents_dict = {}

        raw_data = self.get_intents(bot_id)
        for intent in raw_data:
            for question in intent["questions"]:
                data.append(question['question'])
                intents.append(intent['name'])
            intents_dict[intent['name']] = str(intent['_id'])

        return data, intents, intents_dict

    def get_entities(self, bot_id=2):
        entities = {}
        entities_cursor = self.db.entity.find({"bot_id": bot_id})
        for entity in list(entities_cursor):
            id = str(entity['_id'])
            entities[id] = {
                'name': entity['name'],
                'is_value_restricted': entity['is_value_restricted'],
                'values': entity['values']
            }
        return entities

    def get_entity(self, entity_id):
        entity = self.db.entity.find_one({"_id": ObjectId(entity_id)})
        return {
            '_id': str(entity['_id']),
            'bot_id': entity['bot_id'],
            'name': entity['name'],
            'is_value_restricted': entity['is_value_restricted'],
            'values': entity['values']
        }
        return entity

    def get_tree(self, bot_id=2):
        tree_cursor = self.db.tree.find({"bot_id": bot_id})

        nodes = []
        for item in list(tree_cursor):
            node = {
                '_id': str(item['_id']),
                'parent_node_id': item['parent_node_id'],
                'node_type': item['type'],
                'response': item['response'],
                'result_id': item['result_id'],
                'waits_entity': item['waits_entity_response'],
                'response_type': item['response_type'],
                'response_msg_id': item['response_msg_id']
            }
            nodes.append(node)
        return nodes

    def get_external_source(self, source_id):
        source = self.db.external_integration.find_one({"_id": ObjectId(source_id)})
        return {
            '_id': str(source['_id']),
            'bot_id': source['bot_id'],
            'name': source['name'],
            'external_post_api_url': source['external_post_api_url'],
            'post_message': source['post_message'],
            'type': source['type']
        }
        return source
