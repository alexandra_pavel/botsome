from .csv_adapter import CsvDataRetriever
from .mongo_data_adapter import MongoDataRetriever
from app_constants import Constants


class BaseDataRetriever:
    def __init__(self):
        self.csv_adapter = CsvDataRetriever()
        self.mongo_adapter = MongoDataRetriever()

    def get_training_dat(self, bot_id):
        if Constants.DATA_SOURCE == 'csv':
            return self.csv_adapter.get_intents(bot_id=bot_id)
        elif Constants.DATA_SOURCE == 'mongo':
            return self.mongo_adapter.get_training_data(bot_id=bot_id)

    def get_entities(self, bot_id):
        if Constants.DATA_SOURCE == 'csv':
            return self.csv_adapter.get_entities(bot_id=bot_id)
        elif Constants.DATA_SOURCE == 'mongo':
            return self.mongo_adapter.get_entities(bot_id=bot_id)

    def get_tree(self, bot_id):
        if Constants.DATA_SOURCE == 'csv':
            return self.csv_adapter.get_tree(bot_id=bot_id)
        elif Constants.DATA_SOURCE == 'mongo':
            return self.mongo_adapter.get_tree(bot_id=bot_id)

    def get_entity(self, entity_id):
        if Constants.DATA_SOURCE == 'csv':
            return self.csv_adapter.get_entity(entity_id=entity_id)
        elif Constants.DATA_SOURCE == 'mongo':
            return self.mongo_adapter.get_entity(entity_id=entity_id)

    def get_external_source(self, source_id):
        if Constants.DATA_SOURCE == 'mongo':
            return self.mongo_adapter.get_external_source(source_id=source_id)


