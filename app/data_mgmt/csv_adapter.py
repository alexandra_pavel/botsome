import csv


class CsvDataRetriever:
    def get_intents(self, bot_id=2):
        file_name = "data/classifier-training-data.csv"
        data = []
        intents = []
        intents_dic = {}

        with open(file_name, newline='') as csv_file:
            data_reader = csv.reader(csv_file, delimiter=',', quotechar='"')
            rows_list = []

            for row in data_reader:
                rows_list.append(row)

            for item in rows_list:
                intent_id = int(item[0].strip())
                intent_name = item[1]
                question = item[2]
                data.append(question)
                intents.append(intent_name)
                intents_dic[intent_name] = intent_id

        return data, intents, intents_dic

    def get_entities(self, bot_id=2):
        entities = {}
        with open('data/entities.csv', newline='') as csv_file:
            data_reader = csv.reader(csv_file, delimiter=',', quotechar='"')

            for row in data_reader:
                entities[int(row[0])] = {
                    'name':row[1],
                    'is_value_restricted': row[2],
                    'values': row[3]
                }
        return entities

    def get_tree(self, bot_id=2):
        file_name = 'data/tree.csv'
        with open(file_name, newline='') as csv_file:
            data_reader = csv.reader(csv_file, delimiter=',', quotechar='"')
            rows_list = []
            nodes = []

            for row in data_reader:
                rows_list.append(row)

            for item in rows_list:
                node = {
                    '_id': item[0].strip(),
                    'parent_node_id': item[1].strip(),
                    'type': item[2],
                    'response': item[3],
                    'result_id': item[4].strip(),
                    'waits_entity': item[5].strip()
                }
                nodes.append(node)
        return nodes

    def get_entity(self, entity_id):
        entities = self.get_entities()
        return entities[entity_id]

