from pymongo import MongoClient
from app_constants import Constants
from bson.json_util import dumps


class ChatbotAdminRepository:
    def __init__(self):
        client = MongoClient(Constants.MONGO_CONNECTION)
        self.db = client.botsomedb

    def get_chat_bot(self, bot_id=2):
        chat_bot_cursor = self.db.chatbot.find_one({"bot_id": bot_id})
        return chat_bot_cursor

    def get_intents(self, bot_id=2):
        intents_cursor = self.db.intent.find({"bot_id": bot_id})
        return dumps(intents_cursor)

    def get_intent_questions(self, intent_id="0"):
        intent_question_cursor = self.db.intent_question.find({"intent_id": intent_id})
        return dumps(intent_question_cursor)

    def get_entities(self, bot_id=2):
        entities_cursor = self.db.entity.find({"bot_id": bot_id})
        return dumps(entities_cursor)

    def get_tree(self, bot_id=2):
        tree_cursor = self.db.tree.find({"bot_id": bot_id})
        return dumps(tree_cursor)

    def add_intent(self, intent):
        intent_id = self.db.intent.insert_one(intent)
        return intent_id

    def add_entity(self, entity):
        entity_id = self.db.entity.insert_one(entity)
        return entity_id

    def add_conversation_node(self, node):
        node_id = self.db.tree.insert_one(node)
        return node_id

    def add_intent_question(self, question):
        question_id = self.db.intent_question.insert_one(question)
        return question_id

