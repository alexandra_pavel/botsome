class Node:
    def __init__(self, _id="0", node_type='intent', response='I am sorry. I can not understand.',
                 result_id=0, waits_entity=0, child_nodes=[], parent_node_id=-1,
                 response_type='text', response_msg_id='0'):
        self._id = _id
        self.node_type = node_type
        self.response = response
        self.result_id = result_id
        self.waits_entity = waits_entity
        self.child_nodes = child_nodes
        self.parent_node_id = parent_node_id
        self.response_type = response_type
        self.response_msg_id= response_msg_id

