var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    mongoose = require('mongoose'),
    Intent = require('./api/Models/intentModel'),
    bodyParser = require('body-parser'),
    cors = require('cors');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/botsomedb'); 

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors())

require('./api/routes/botsomeRoutes')(app);

app.listen(port);

console.log('botsome admin RESTful API server started on: ' + port);
