'use strict';
var mongoose = require('mongoose');

var ChatMessageSchema = new mongoose.Schema({ 
    bot_id: {
      type: Number,
      Required: 'Kindly enter bot id of the message.'
    },
    sender: {
        type: String,
        Required: 'Kindly enter the sender of the message.'
    },
    recipient: {
        type: String,
        Required: 'Kindly enter the recipient of the message.'
    },
    message: {
        type: String,
        Required: 'Kindly enter the message text.'
    },
    node_result_id: {
        type: String
    },
    node_type: {
        type:String
    },
    created_date: {
        type: Date,
        default: Date.now
    }
},
{collection : 'chat_message'});

var ChatMessage = mongoose.model('chat_message', ChatMessageSchema);

module.exports = {
    ChatMessage : ChatMessage
};