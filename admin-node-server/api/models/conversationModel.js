'use strict';
var mongoose = require('mongoose');

var ConversationNodeSchema = new mongoose.Schema({ 
  bot_id: {
      type: Number,
      Required: 'Kindly enter bot id of the conversation node.'
  },
  parent_node_id: {
    type: String,
    required: "Plase add the parent node id."
  },
  type: {
      type: String,
      required: "Please add the node type."
  },
  response: {
      type: String,
      default: ""
  },
  result_id: {
      type: String,
      required: "Please add a result node id."
  },
  waits_entity_response: {
      type: String,
      default: "0"
  },
  response_type: {
    type: String
  },
  response_msg_id: {
      type: String
  },
  created_date: {
    type: Date,
    default: Date.now
  }
},
{collection : 'tree'});


var  ConversationNode = mongoose.model('tree', ConversationNodeSchema);


module.exports = {
    ConversationNode : ConversationNode
};

