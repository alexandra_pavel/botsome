'use strict';
var mongoose = require('mongoose');

var EntitySchema = new mongoose.Schema({ 
    bot_id: {
      type: Number,
      Required: 'Kindly enter bot id of the entity.'
    },
    entity_id: {
       type: Number
    },
    name: {
      type: String,
      Required: 'Kindly enter the name of the entity.'
    },
    is_value_restricted: {
         type: Boolean,
         default: false
    },
    values: {
        type: String
    },
    created_date: {
        type: Date,
        default: Date.now
    }
},
{collection : 'entity'});

var Entity = mongoose.model('entity', EntitySchema);

module.exports = {
    Entity : Entity
};