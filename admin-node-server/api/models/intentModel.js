'use strict';
var mongoose = require('mongoose');

var IntentSchema = new mongoose.Schema({ 
  bot_id: {
      type: Number,
      Required: 'Kindly enter bot id of the intent.'
  },
  name: {
    type: String,
    Required: 'Kindly enter the name of the intent.'
  },
  created_date: {
    type: Date,
    default: Date.now
  }
},
{collection : 'intent'});

var Intent;
if (mongoose.models.intent) {
  Intent = mongoose.model('intent');
} else {
  Intent = mongoose.model('intent', IntentSchema);
}

module.exports = {
    Intent : Intent
};

