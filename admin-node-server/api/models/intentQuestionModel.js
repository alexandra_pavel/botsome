'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var IntentQuestionSchema = new mongoose.Schema({ 
  intent_id: {
      type: Number,
      Required: 'Kindly enter intent id of the question.'
  },
  intent_obj_id:{
      type: ObjectId,
      Required: 'Kindly enter intent id of the question.'
  },
  question: {
    type: String,
    Required: 'kindly enter the question.'
  },
  created_date: {
    type: Date,
    default: Date.now
  }
},
{collection : 'intent_question'});

var IntentQuestion = mongoose.model('intentQuestion', IntentQuestionSchema);

module.exports = {
    IntentQuestion : IntentQuestion
};

