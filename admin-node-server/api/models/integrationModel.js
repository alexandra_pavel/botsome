'use strict';
var mongoose = require('mongoose');

var IntegrationSchema = new mongoose.Schema({ 
    bot_id: {
      type: Number,
      Required: 'Kindly enter bot id of the integration.'
    },
    name: {
      type: String,
      Required: 'Kindly enter the name of the integration.'
    },
    external_post_api_url: {
        type: String
    },
    post_message: {
        type: String
    },
    type: {
        type: String
    }
},
{collection : 'external_integration'});

var Integration = mongoose.model('integration', IntegrationSchema);

module.exports = {
    Integration : Integration
};