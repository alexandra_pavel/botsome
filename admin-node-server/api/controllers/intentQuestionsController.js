'use strict';

var mongoose = require('mongoose'),
  IntentQuestion = require('../models/intentQuestionModel').IntentQuestion;
var ObjectId = mongoose.Types.ObjectId;

exports.list_all_question = function(req, res) {
  IntentQuestion.find({}, function(err, questions) {
    if (err){
        res.status(500);
        res.send(err);
    }
    res.json(questions);
  });
};

exports.create_question = function(req, res) {
  var new_question = new IntentQuestion(req.body);
  new_question.intent_obj_id = ObjectId(req.body.intent_obj_id);

  new_question.save(function(err, question) {
    if (err){
      res.status(500);
      res.send(err);
    }
    res.json(question);
  });
};

exports.update_question = function(req, res) {
  var id = req.params.questionId;

  if(id != "0"){
    id = ObjectId(req.params.questionId); 
    IntentQuestion.findOne({'_id': id}, function(err, question){
        if(!err){
            if(question){
                
    console.log('Updating question with id: ' + req.params.questionId + ', question: ' + req.body.question);
                question.question = req.body.question;
                question.intent_obj_id = ObjectId(req.body.intent_obj_id);
                question.save(function(err){
                    if(err){
                        res.status(500);
                        res.send(err);
                    }else{
                        res.json({message: 'Saved question.'});
                    }
                });               
            }
        }else{ 
            console.log('error fetching question '); 
            res.status(500);
            res.json({message: err});
             
        }
    });
  }else{
      
    console.log('Adding question : '+ req.body.question);
        var question = new IntentQuestion();
        console.log('new question id: ' + question._id);
        question.question = req.body.question;
        question.created_date = Date.now();
        question.intent_id = req.body.intent_id;
        question.intent_obj_id = ObjectId(req.body.intent_obj_id);
        question.save(function(err){
            if(err){
                res.status(500);
                res.send(err);
            }else{
                res.json({"newId": question._id});
            }
        });
  }
};

exports.delete_question = function(req, res) {
  IntentQuestion.remove({
    _id: req.params.questionId
  }, function(err, question) {
    if (err){
        res.status(500);
        res.send(err);
    }
    res.json({ message: 'Intent question successfully deleted' });
  });
};


