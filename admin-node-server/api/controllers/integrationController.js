'use strict';

var mongoose = require('mongoose'),
  Integration = require('../models/integrationModel').Integration;
  var ObjectId = mongoose.Types.ObjectId;

exports.list_all_integrations = function(req, res) {
  var botId = req.params.botId;
  
  Integration.find({'bot_id':botId}, function(err, integrations) {
    if (err){
        res.status(500);
        res.send(err);
    }
    res.json(integrations);
  });
};

exports.create_integration = function(req, res) {
    var new_Integration = new Integration();
    new_Integration.bot_id = req.body.bot_id;
    new_Integration.name = req.body.name;
    new_Integration.external_post_api_url = req.body.external_post_api_url;
    new_Integration.post_message = req.body.post_message;
    new_Integration.type = req.body.type;
    
  console.log('Adding new integration with name ' + new_Integration.name);
  new_Integration.save(function(err) {
    if (err){
      res.status(500);
      res.send(err);
    }
    res.json({"newId": new_Integration._id});
  });
};

exports.update_integration = function(req, res) {
  console.log('Updating integration with name ' + req.body.name);
  var id = ObjectId(req.params.integrationId);

  Integration.findOne({'_id': id}, function(err, integration){
        if(!err){
            if(integration){                
                console.log('Updating integration with id: ' + req.params.integrationId + ', name: ' + req.body.name);
                integration.bot_id = req.body.bot_id;
                integration.name = req.body.name;
                integration.external_post_api_url = req.body.external_post_api_url;
                integration.post_message = req.body.post_message;
                integration.type = req.body.type;
                
                integration.save(function(err){
                    if(err){
                        res.status(500);
                        res.send(err);
                    }else{
                        res.json({message: 'Saved integration.'});
                    }
                });               
            }
        }
  });
};

exports.delete_integration = function(req, res) {
  Integration.remove({
    _id: req.params.integrationId
  }, function(err, entity) {
    if (err){
      res.status(500);
      res.send(err);
    }
    res.json({ message: 'Integration successfully deleted' });
  });
};