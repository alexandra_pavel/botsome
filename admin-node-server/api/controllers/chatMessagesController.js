'use strict';

var mongoose = require('mongoose'),
  ChatMessage = require('../models/chatMessageModel').ChatMessage;
  var ObjectId = mongoose.Types.ObjectId;

exports.list_last_messages = function(req, res) {
    var botId = req.params.botId;

    ChatMessage
    .find({'bot_id': botId})
    .sort({'created_date': -1})
    .limit(20)
    .exec(function(err, messages) {
     if (err){
        res.status(500);
        res.send(err);
    }
    res.json(messages);
    });
}


exports.create_chat_message = function(req, res) {
    var msg = new ChatMessage();
    msg.bot_id = req.body.bot_id;
    msg.sender = req.body.sender;
    msg.recipient = req.body.recipient;
    msg.message = req.body.message;
    msg.node_result_id = req.body.node_result_id;
    msg.node_type = req.body.node_type;
    msg.created_date = Date.now();

  
  msg.save(function(err) {
    if (err){
         res.status(500);
        res.send(err);
      
        console.log('Error while addind new chat message ' + err);
    }
    res.json({"newId": msg._id});
    console.log('Added new chat message with id.' + msg._id);
  });
};

var startDate = new Date(new Date().getTime() - (7 * 24 * 60 * 60 * 1000));

exports.list_weekly_messages = function(req, res) {
    var botId = req.params.botId;

    ChatMessage
    .find({'bot_id': botId, "created_date": { "$gte": startDate }})
    .exec(function(err, messages) {
     if (err){
        res.status(500);
        res.send(err);
    }
    res.json(messages);
    });
}