'use strict';

var mongoose = require('mongoose'),
  Entity = require('../models/entityModel').Entity;
  var ObjectId = mongoose.Types.ObjectId;

exports.list_all_entities = function(req, res) {
  var botId = req.params.botId;
  
  Entity.find({'bot_id':botId}, function(err, entities) {
    if (err){
        res.status(500);
        res.send(err);
    }
    res.json(entities);
  });
};

exports.create_entity = function(req, res) {
  var new_entity = new Entity();
  new_entity.entity_id = req.body.entity_id;
  new_entity.bot_id = req.body.bot_id;
  new_entity.name = req.body.name;
  new_entity.is_value_restricted = req.body.is_value_restricted;
  new_entity.values = req.body.values;

  console.log('Adding new entity with name ' + new_entity.name);
  new_entity.save(function(err) {
    if (err){
      res.status(500);
      res.send(err);
    }
    res.json({"newId": new_entity._id});
  });
};


exports.read_entity = function(req, res) {
  Entity.findById(req.params.entityId, function(err, entity) {
    if (err){
      res.status(500);
      res.send(err);
    }
    res.json(entity);
  });
};

exports.update_entity = function(req, res) {
  console.log('Updating entity with name ' + req.body.name);
  var id = ObjectId(req.params.entityId);

  Entity.findOne({'_id': id}, function(err, entity){
        if(!err){
            if(entity){                
                console.log('Updating entity with id: ' + req.params.entityId + ', name: ' + req.body.name);
                entity.name = req.body.name;
                entity.is_value_restricted = req.body.is_value_restricted;
                entity.values = req.body.values;
                
                entity.save(function(err){
                    if(err){
                        res.status(500);
                        res.send(err);
                    }else{
                        res.json({message: 'Saved entity.'});
                    }
                });               
            }
        }
  });
};

exports.delete_entity = function(req, res) {
  Entity.remove({
    _id: req.params.entityId
  }, function(err, entity) {
    if (err){
      res.status(500);
      res.send(err);
    }
    res.json({ message: 'Entity successfully deleted' });
  });
};