'use strict';

var mongoose = require('mongoose'),
  Intent = require('../models/intentModel').Intent;

exports.list_all_intents = function(req, res) {
  var botId = req.params.botId;

  Intent.find({'bot_id': botId}, function(err, intents) {
    if (err)
      res.send(err);
    res.json(intents);
  });
};

exports.create_intent = function(req, res) {
  var new_intent = new Intent();
  
  console.log('Adding new intent with id: ' + new_intent._id);
  new_intent.bot_id = req.body.bot_id;
  new_intent.intent_id = req.body.intent_id;
  new_intent.name = req.body.name;

  new_intent.save(function(err) {
    if (err){
      res.status(500);
      res.send(err);
    }
    res.json({"newId": new_intent._id});
  });
};

exports.read_intent = function(req, res) {
  Intent.findById(req.params.intentId, function(err, intent) {
    if (err)
      res.send(err);
    res.json(intent);
  });
};

exports.update_intent = function(req, res) {
  Intent.findOneAndUpdate(req.params.intentId, req.body, {new: true}, function(err, intent) {
    if (err)
      res.send(err);
    res.json(intent);
  });
};

exports.delete_intent = function(req, res) {
  Intent.remove({
    _id: req.params.intentId
  }, function(err, intent) {
    if (err)
      res.send(err);
    res.json({ message: 'Intent successfully deleted' });
  });
};



