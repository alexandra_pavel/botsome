'use strict';

var mongoose = require('mongoose'),
  ConversationNode = require('../models/conversationModel').ConversationNode;
  var ObjectId = mongoose.Types.ObjectId;

exports.list_all_nodes = function(req, res) {
    var botId = req.params.botId;
  ConversationNode.find({'bot_id': botId}, function(err, nodes) {
    if (err){
        res.status(500);
        res.send(err);
    }
    res.json(nodes);
  });
}


exports.create_node = function(req, res) {
    var node = new ConversationNode();
    node.bot_id = req.body.bot_id;
    node.parent_node_id = req.body.parent_node_id;
    node.type = req.body.type;
    node.response = req.body.response;
    node.result_id = req.body.result_id;
    node.waits_entity_response = req.body.waits_entity_response;
    node.response_msg_id = req.body.response_msg_id;
    node.response_type = req.body.response_type;

  console.log('Adding new node with parent id ' + node.parent_node_id);
  node.save(function(err) {
    if (err){
      res.status(500);
      res.send(err);
    }
    res.json({"newId": node._id});
  });
};

exports.update_node = function(req, res) {
  var id = ObjectId(req.params.nodeId);

  ConversationNode.findOne({'_id': id}, function(err, node){
        if(!err){
            if(node){                
                console.log('Updating conversation node with id ' + req.params.nodeId);
                node.bot_id = req.body.bot_id;
                node.parent_node_id = req.body.parent_node_id;
                node.type = req.body.type;
                node.response = req.body.response;
                node.result_id = req.body.result_id;
                node.waits_entity_response = req.body.waits_entity_response;
                node.response_msg_id = req.body.response_msg_id;
                node.response_type = req.body.response_type;
                
                node.save(function(err){
                    if(err){
                        res.status(500);
                        res.send(err);
                    }else{
                        res.json({message: 'Saved node.'});
                    }
                });               
            }
        }
  });
};

exports.delete_node = function(req, res) {
  ConversationNode.remove({
    _id: req.params.nodeId
  }, function(err, node) {
    if (err){
      res.status(500);
      res.send(err);
    }
    res.json({ message: 'Conversation node successfully deleted.' });
  });
};
