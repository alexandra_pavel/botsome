'use strict';

module.exports = function(app) {
  var entitiesCtrl = require('../controllers/entityController');
  var intentsCtrl = require('../controllers/intentsController');
  var questionsCtrl = require('../controllers/intentQuestionsController');
  var conversationCtrl = require('../controllers/conversationController');
  var chatMsgCtrl = require('../controllers/chatMessagesController');
  var integrCtrl = require('../controllers/integrationController');

  var plansCtrl = require('../controllers/plansController');

  // intents Routes
  app.route('/api/:botId/intents')
    .get(intentsCtrl.list_all_intents);

  app.route('/api/intents')
    .post(intentsCtrl.create_intent);

  app.route('/api/intents/:intentId')
    .get(intentsCtrl.read_intent)
    .put(intentsCtrl.update_intent)
    .delete(intentsCtrl.delete_intent);

  // entities Routes
  app.route('/api/:botId/entities')
    .get(entitiesCtrl.list_all_entities);

  app.route('/api/entities')
    .post(entitiesCtrl.create_entity);

  app.route('/api/entities/:entityId')
    .get(entitiesCtrl.read_entity)
    .put(entitiesCtrl.update_entity)
    .delete(entitiesCtrl.delete_entity);

  //questions Routes
  app.route('/api/questions')
    .get(questionsCtrl.list_all_question)
    .post(questionsCtrl.create_question);

  app.route('/api/questions/:questionId')
    .put(questionsCtrl.update_question)
    .delete(questionsCtrl.delete_question);

  //conversation Routes
  app.route('/api/:botId/conversationNodes')
    .get(conversationCtrl.list_all_nodes);
    
  app.route('/api/conversationNodes')
    .post(conversationCtrl.create_node);

  app.route('/api/conversationNodes/:nodeId')
    .put(conversationCtrl.update_node)
    .delete(conversationCtrl.delete_node);

  //chat messages Routes
  app.route('/api/:botId/chatMessages')
    .get(chatMsgCtrl.list_last_messages);

  app.route('/api/chatMessages')   
    .post(chatMsgCtrl.create_chat_message);

  app.route('/api/:botId/weeklyMessages')
    .get(chatMsgCtrl.list_weekly_messages);

    //integration routes
  app.route('/api/:botId/integrations')
    .get(integrCtrl.list_all_integrations);

  app.route('/api/integrations')
    .post(integrCtrl.create_integration);

  app.route('/api/integrations/:integrationId')
    .put(integrCtrl.update_integration)
    .delete(integrCtrl.delete_integration);

  app.route('/api/getPlans')
    .post(plansCtrl.available_plan);
};
